<?php
function user_account_information_page(){
	return drupal_get_form('user_account_information_form');
}




function user_account_information_form($form, &$form_state) {
	drupal_set_title('');
	drupal_add_css(drupal_get_path('module', 'user_accounts') . '/css/user_accounts.css');
	drupal_add_js(drupal_get_path('module', 'user_accounts') . '/js/mask.js');
	drupal_add_js(drupal_get_path('module', 'user_accounts') . '/js/user_accounts.js');
	

	$userform=array();
	$roles = user_roles ();
	$countrieslist=array('840'=>'United States');
	$usStatesObject = uc_zone_select();
	$usStatesList = $usStatesObject['#options'];
	
	$userform['useraccountinformation']=array(
			'#type'=>'markup',
			'#markup'=>'<legend><h1>Account Information </h1></legend>',
	);
	
	$userform['account_name']=array(
			'#id'=>'account_name',
			'#type'=>'textfield',
			'#title'=>'Account Name',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	$userform['account_name_check']=array(
			'#type' => 'button',
			'#value' => t('Check Availability'),
			'#ajax' => array(
				'callback' => 'check_user'
			),
			'#prefix'=>'<div style="padding-top:20px;">',
			'#suffix'=>'</div></div>',
	);
	
	$userform ['username_status'] = array (
			'#type' => 'markup',
			'#markup' => '<div id="usernamestatusdiv"></div>'
	);
	
	
	
	
	$userform['accout_type']=array(
			'#id'=>'accout_type',
			'#type'=>'select',
			'#options'=>$roles,
			'#title'=>'Account Type',
			'#maxlength' => 64,
			'#prefix'=>'<div>',
			'#suffix'=>'</div>',
	);
	
	
	$userform['title']=array(
		'#id'=>'title',
		'#type'=>'textfield',
		'#title'=>'Title',
		'#maxlength' => 64,
		'#prefix'=>'<div class="formalign"><div class="formNamefields">',
		'#suffix'=>'</div>',
		'#attributes'=>array('size'=>array('25')),
	);
	
	$userform['first_name']=array(
		'#id'=>'first_name',
		'#type'=>'textfield',
		'#title'=>'First Name*',
		'#maxlength' => 64,
		'#prefix'=>'<div class="formNamefields">',
		'#suffix'=>'</div>',
		'#attributes'=>array('size'=>array('25')),
	);
	
	$userform['last_name']=array(
		'#id'=>'last_name',
		'#type'=>'textfield',
		'#title'=>'Last Name*',
		'#maxlength' => 64,
			'#prefix'=>'<div class="formNamefields">',
			'#suffix'=>'</div></div>',
			'#attributes'=>array('size'=>array('25')),
	);
	
	$userform['company_name']=array(
			'#id'=>'company_name',
			'#type'=>'textfield',
			'#title'=>'Company Name',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign">',
			'#suffix'=>'</div>',
			
	);
	
	
	$userform['usercontactinformation']=array(
			'#type'=>'markup',
			'#markup'=>'<h1><legend>Contact Information</legend></h1>',
	);
	
	
	$userform['email']=array(
		'#id'=>'email',
		'#type'=>'textfield',
		'#title'=>'Email',
		'#maxlength' => 250,
		'#attributes'=>array('size'=>array('25')),
		'#prefix'=>'<div>',
		'#suffix'=>'</div>',
	);
	
	
	$userform['web_address']=array(
			'#id'=>'web_address',
			'#type'=>'textfield',
			'#title'=>'Web address',
			'#maxlength' => 250,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div>',
			'#suffix'=>'</div>',
	);
	
	$userform['work_phone']=array(
			'#id'=>'work_phone',
			'#type'=>'textfield',
			'#title'=>'Work Phone',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	$userform['mobile_phone']=array(
			'#id'=>'mobile_phone',
			'#type'=>'textfield',
			'#title'=>'Mobile Phone',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	$userform['fax']=array(
		'#id'=>'fax',
		'#type'=>'textfield',
		'#title'=>'Fax',
		'#maxlength' => 64,
		'#prefix'=>'<div class="formNamefields">',
		'#suffix'=>'</div></div>',
		'#attributes'=>array('size'=>array('25')),
	);
	
	
	$userform['useraddressformation']=array(
			'#type'=>'markup',
			'#markup'=>'<h1><legend>Address Information </legend></h1>',
	);
	
	
	
	$userform['address_1']=array(
			'#id'=>'address_1',
			'#type'=>'textfield',
			'#title'=>'Street Address',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['address_2']=array(
			'#id'=>'address_2',
			'#type'=>'textfield',
			'#title'=>'Address 2',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	
	
	$userform['city']=array(
			'#id'=>'city',
			'#type'=>'textfield',
			'#title'=>'City',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['state']=array(
			'#id'=>'state',
			'#type'=>'select',
			'#title'=>'State/Province',
			'#options' => $usStatesList,
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	
	
	$userform['postal_code']=array(
			'#id'=>'postal_code',
			'#type'=>'textfield',
			'#title'=>'Zip/Postal Code',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['country']=array(
			
			'#type'=>'select',
			'#options' => $countrieslist,
			'#id'=>'country',
			'#title'=>'Country',
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	
	
	
	$userform['userhomeinformation']=array(
			'#type'=>'markup',
			'#markup'=>'<h1><legend></legend></h1>',
	);
	
	
	
	
	$userform['home_address_1']=array(
			'#id'=>'home_address_1',
			'#type'=>'textfield',
			'#title'=>'Home Address 1',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['home_address_2']=array(
			'#id'=>'home_address_2',
			'#type'=>'textfield',
			'#title'=>'Home Address 2',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	
	
	$userform['home_city']=array(
			'#id'=>'home_city',
			'#type'=>'textfield',
			'#title'=>'Home City',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['home_zone']=array(
			'#id'=>'home_zone',
			'#type'=>'select',
			'#title'=>'State/Province',
			'#options' => $usStatesList,
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	
	
	$userform['home_postal_code']=array(
			'#id'=>'home_postal_code',
			'#type'=>'textfield',
			'#title'=>'Home Postal Code',
			'#maxlength' => 64,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	$userform['home_country']=array(
			'#id'=>'home_country',
			'#type'=>'select',
			'#title'=>'Home Country',
			'#options' => $countrieslist,
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	

	$userform['home_phone']=array(
			'#id'=>'home_phone',
			'#type'=>'textfield',
			'#title'=>'Home Phone',
			'#maxlength' => 250,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div>',
			'#suffix'=>'</div>',
	);
	

	$userform['birth_date']=array(
			'#id'=>'birth_date',
			'#type'=>'textfield',
			'#title'=>'Birth Date',
			'#maxlength' => 250,
			'#attributes'=>array('size'=>array('25')),
			'#prefix'=>'<div>',
			'#suffix'=>'</div>',
	);
	
	
	
	$userform['useraccountsubmit']=array(
			'#id'=>'useraccountsubmit',
			'#type'=>'submit',
			'#value'=>'Create Account',
			'#prefix'=>'<div class="formalign"><div class="formNamefields">',
			'#suffix'=>'</div>',
	);
	
	
	
	
	$userform['useraccountcancel']=array(
			'#id'=>'useraccountcancel',
			'#type'=>'submit',
			'#value'=>'Cancel',
			'#prefix'=>'<div>',
			'#suffix'=>'</div></div>',
	);
	

	return $userform;
	
}

function user_account_information_form_validate($form, &$form_state) {
	if ($error = user_validate_name($form_state['values']['account_name'])) {
		form_set_error('account_name', $error);
	}
	elseif ((bool) db_select('users')->fields('users', array('uid'))->condition('name', db_like($form_state['values']['account_name']), 'LIKE')->range(0, 1)->execute()->fetchField()) {
	form_set_error('account_name', t('The name %name is already taken.', array('%name' => $form_state['values']['account_name'])));
	}


	$mail = trim($form_state['values']['email']);
	if ($error = user_validate_mail($form_state['values']['email'])) {
		form_set_error('email', $error);
	}
	elseif ((bool) db_select('users')->fields('users', array('uid'))->condition('mail', db_like($form_state['values']['email']), 'LIKE')->range(0, 1)->execute()->fetchField()) {
	form_set_error('email', t('The e-mail address %email is already taken.', array('%email' => $form_state['values']['email'])));
	}

}


function user_account_information_form_submit($form, &$form_state){
	
	$account_name=$form_state['values']['account_name'];
	$accout_type=$form_state['values']['accout_type'];
	$title=$form_state['values']['title'];
	$first_name=$form_state['values']['first_name'];
	$last_name=$form_state['values']['last_name'];
	$company_name=$form_state['values']['company_name'];
	$email=$form_state['values']['email'];
	$web_address=$form_state['values']['web_address'];
	$work_phone=$form_state['values']['work_phone'];
	$mobile_phone=$form_state['values']['mobile_phone'];
	$fax=$form_state['values']['fax'];
	$address_1=$form_state['values']['address_1'];
	$address_2=$form_state['values']['address_2'];
	$city=$form_state['values']['city'];
	$state=$form_state['values']['state'];
	$postal_code=$form_state['values']['postal_code'];
	$country=$form_state['values']['country'];
	$home_address_1=$form_state['values']['home_address_1'];
	$home_address_2=$form_state['values']['home_address_2'];
	$home_city=$form_state['values']['home_city'];
	$home_zone=$form_state['values']['home_zone'];
	$home_postal_code=$form_state['values']['home_postal_code'];
	$home_country=$form_state['values']['home_country'];
	$home_phone=$form_state['values']['home_phone'];
	$birth_date=$form_state['values']['birth_date'];
	
	$roles = user_roles();
		
	$selectedRole=array();
	foreach($roles as $key => $value){
		if($key==$accout_type){
			$selectedRole[$key]=$value;
		}
	}

	$password=user_password ( 6 );
	$newUserData = array(
			'name' => $account_name,
			'pass' => $password, // note: do not md5 the password
			'mail' => $email,
			'status' => 1,
			'timezone' => variable_get('date_default_timezone'),
			'init' => $email,
			'roles'=>$selectedRole,
			'data' => '',
			);
	
	//the first parameter is left blank so a new user is created
	$account = user_save('', $newUserData);
	
	// If you want to send the welcome email, use the following code
	
	// Manually set the password so it appears in the e-mail.
	$account->password = $newUserData['pass'];
	
	// Send the e-mail through the user module.
	drupal_mail('user', 'register_no_approval_required', $email, NULL, array('account' => $account), variable_get('site_mail', '<a href="mailto:noreply@example..com">noreply@example..com</a>'));
	
	
	
// 		$new_user = user_save($account, $newUserData);
		$userid=$account->uid;
		
		
		$id = db_insert('user_accounts')
		->fields(array(
				'customer_id' => $userid,
				'first_name' => $first_name,
				'last_name' => $last_name,
				'company_name' => $company_name,
				'web_address' => $web_address,
				'work_phone' => $work_phone,
				'mobile_phone' => $mobile_phone,
				'fax' => $fax,
				'address_1' => $address_1,
				'address_2' => $address_2,
				'city' => $city,
				'state' => $state,
				'postal_code' => $postal_code,
				'country' => $country,
				'home_address_1' => $home_address_1,
				'home_address_2' => $home_address_2,
				'home_city'=>$home_city,
				'home_zone' => $home_zone,
				'home_postal_code' => $home_postal_code,
				'home_country' => $home_country,
				'home_phone' => $home_phone,
				'birth_date' => $birth_date,
				
		))
		->execute();
		
		
		
// 		$params = array(
// 				'subject' => t('Login Information'),
// 				'body' => t("Data"),
// 				'user' => $account_name,
// 				'pass' => $password
// 		);
// 		$to = $email;
// 		drupal_mail("email_system", "wwc_useraccountemail", $to, language_default(), $params, "satya.webdeveloper@gmail.com");
		
	
	
}


function check_user($form, &$form_state) {
	$commands = array ();
	$name = $form_state ['values'] ['account_name'];
	
	// Validation
	if (isset ( $name )) {
		if ($error = user_validate_name ( $name )) {
	
			$data = $error;
		} elseif (( bool ) db_select ( 'users' )->fields ( 'users', array (	'uid'	) )->condition ( 'name', db_like ( $name ), 'LIKE' )->range ( 0, 1 )->execute ()->fetchField ()) {
			$data = 'The name ' . $name . ' is already taken!.';
			$commands [] = ajax_command_invoke('#account_name', 'attr', array('value', ''));
			$commands [] = ajax_command_replace ("#usernamestatusdiv", "<div id='usernamestatusdiv'><span class='error'>" . $data . "</span></div>" );
				
		}else{
			$data = 'The name ' . $name . ' is available!.';
			$commands [] = ajax_command_replace ( "#usernamestatusdiv", "<div id='usernamestatusdiv'><span class=''>" . $data . "</span></div>" );
				
		}
	}
	
	//var_dump($data); exit();
	return array (
			'#type' => 'ajax',
			'#commands' => $commands
	);
	
	
}
