<script defer src="<?php echo base_path() . path_to_theme() ?>/js/jquery.bxslider.js"></script>
<link rel="stylesheet" href="<?php echo base_path() . path_to_theme() ?>/css/jquery.bxslider.css" type="text/css" />


<link rel="stylesheet" href="<?php echo base_path() . path_to_theme() ?>/magnific-popup/magnific-popup.css" type="text/css" />

<script defer src="<?php echo base_path() . path_to_theme() ?>/magnific-popup/jquery.magnific-popup.js"></script>

<script>

    function privateEventsDetails(nid){
        window.location.href = '<?php echo base_path() ?>eventscruise/' + nid;
    
    }

    $(function() {
        var selectedMenuItem = '<?php echo $selectedmenuelement; ?>';
        //
        $("#weddingsRequestProposal").click(function() {
            $("#myModalrequest").modal('show');
        });
        // menu active script
        // To do active Tab
        $(".responsiveWeb li").removeClass("active");
        $('.privateevents').addClass("active");

        $('.weddingcategory li .sliderBox').mouseover(function(e) {
            $(this).children(".sliderBoxout").hide();
            $(this).children(".sliderBoxover").show();
        });

        $('.weddingcategory li .sliderBox').mouseout(function(e) {
            $(this).children(".sliderBoxout").show();
            $(this).children(".sliderBoxover").hide();
        });
        $('.weddingcategory').bxSlider({
        	 infiniteLoop: false,
             hideControlOnEnd: true,
             minSlides: 2,
             maxSlides: 3,
             slideWidth: 300,
             slideHeight: 300,
             slideMargin: 10,
             touchEnabled: true
       });

        $('.eventsbxslider').magnificPopup({
            delegate: 'a',
            closeMarkup:'<button title="Close (Esc)" type="button" class="mfp-close"><div class="mfp-close close-text">Close</div></button>',
        
            // child items selector, by clicking on it popup will open
            gallery: {
                enabled: true
            }
            //  type:'image'
  
            // other options
        });


        $('.eventsbxslider').bxSlider({
            infiniteLoop: false,
            hideControlOnEnd: true,
            minSlides: 1,
            maxSlides: 5,
            slideWidth: 300,
            slideHeight: 250,
            slideMargin: 10,
            touchEnabled: true
        });
        
        $('.responsiveMobile select').change(function() {
            window.location.href = '<?php echo base_path() ?>privateevents/category/' + $(this).val();
        });

    });
</script>



<div class="cruiseDetailinfo privateEventsWeddings">
    <div class="row-fluid">
        <div class="span12">
           
            <div class="detailBackLink"><a href="<?php echo base_path()?>privateevents#<?php echo $masterpagetitle; ?>"><img src="<?php echo base_path() . path_to_theme(); ?>/images/back_arrow.png" /> BACK TO <?php echo $masterpagetitle; ?></a></div>
            <div class="row-fluid">
                
                <div class="span12">
                    <div class="weddingTitle"><?php print $masterpagetitle; ?></div>




                    <!--sub Menu links Area-->

                    <div class="responsiveWeb subMenu">

                        <?php print $weddingtabmenu; ?>
                    </div>

                    <!--End Of Sub Menu Links Area-->




                </div>

            </div><!-- row-fluid end -->

        </div>
    </div>
</div>





<!--submenu in mobile view-->
<div class="responsiveMobile subMenu">
    <select>
        <?php print $mobilemenu ?>
    </select>
</div>




<!--doe-->


<!-- sunset down slider start here -->
<div class="row-fluid">
    <div class="span12">
        <!-- sunsetDownslider start here -->
        <div class="sunsetDownslider">

         <img src="<?php print base_path() . drupal_get_path('module', 'privateevents') . '/images/Waterways_Weddings.png'; ?>" class="imagefixedwd" />
   

            <?php if($gallery){ ?>
            <div class="row-fluid eventsDownslidermargin privateEventsGallery">
                <ul class="eventsbxslider">
                    <?php print $gallery ?>
                </ul>
            </div>
            <?php } ?>
         
            <!--short description banner gets loads here-->
            <?php if($gallery){ ?>
            <div class="weddingDownsliderbox" style="display: none;">
            <?php }else{ ?>
            <div class="weddingDownsliderbox">
            <?php } ?>    
                <div class="sunsetDownsliderInner">
                    <div class="row-fluid">
                        <div class="span2 sunsetLinePadding"><div class="line"></div></div>
                        <div class="span8"><div class="sliderCaption"><?php print $eventscategorytitle; ?></div></div>
                        <div class="span2 sunsetLinePadding"><div class="line"></div></div>
                    </div>

                    <div class="sliderInnercontent">
                        <div class="weddingshortdescription"> <?php print $shortdescription; ?></div>
                        <div class="seeFleet">
<!--                            <a href="<?php print base_path() .$overviewlink; ?>">See the fleet</a></div>-->
                            <a href="<?php print base_path() .'aboutusvendors'; ?>">Vendors</a>
                        <!--<img src="<?php // print base_path() . drupal_get_path('module', 'privateevents') . '/images/dot.png'; ?>" />-->
                        <div class="request" id="weddingsRequestProposal"><a href="#">request for proposal</a></div>
                    </div>

                    <div class="row-fluid" id="routeMaplinesholder">
                        <div class="span5"><div class="line"></div></div>
                        <div class="span2" align="center"><img src="<?php print base_path() . drupal_get_path('module', 'cruisesdetailview') . '/images/banner-icon-img.png'; ?>" /></div>
                        <div id="line2routemap" class="span5"><div class="line"></div></div>
                    </div>
                </div>
            </div>
                <!-- short description content widget ends here -->

            </div>
        <!-- sunsetDownslider end here -->
    </div>
</div>
<!-- sunset down slider end here -->

<!--seccode-->
<div class="container">
    <div class="contentbox-inner">

        <div class="row-fluid tiles">
            <div class="span12">
                <ul class="weddingcategory">
                    <?php print $categoryslider; ?>

                </ul>
            </div>
        </div>
        
    </div>

</div>
