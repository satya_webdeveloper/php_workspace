<?php

/**
 * @file
 * Handles the navigation of the Booking Fomr
 */

/**
 * Generate a header which informs the user of which stage they're on.
 * 
 */
function booking_get_header($form, &$form_state) {
    
    //Getting the Item Details
    $cruiseitemdetails = $_SESSION['bookingInput'];
    $cruiseitemId = $cruiseitemdetails->eventid;
    $bookingdate = $cruiseitemdetails->date;
    $bookingdate = strtotime($bookingdate);
    $bookingdate = date("F d, Y", $bookingdate);
	
	//using cruise Item Id loading the Complete Item Details
    $cruisedetails = cruisedetailsWithportdetails($cruiseitemId);
    $form_state['stage'] = isset($form_state['stage']) ? $form_state['stage'] : 1;
	$form_stages = array (
			'TICKETS' => 1,
			'ENHANCEMENTS' => 2,
			'PAYMENT' => 3,
			'REVIEW_ORDER' => 4 
	);

    if (isset ( $form_stages [$form_state ['stage']] )) {
		$current_step = $form_stages [$form_state ['stage']];
	} else {
		$current_step = 1;
	}

    $stages = array(
        1 => array('data' => 'TICKETS'),
        2 => array('data' => 'ENHANCEMENTS'),
        3 => array('data' => 'PAYMENT'),
        4 => array('data' => 'REVIEW_ORDER'),
    );

    $stages[$current_step]['class'] = array('active');


    $attributes = array(
        'id' => 'my-custom-listing',
        'class' => 'custom-class another-custom-class', // a string or indexed (string) array with the classes for the list tag
    );

    $form['header'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div class="contentbox-inner mobilebookingContainer"><div class="row-fluid"><div class="span12">',
        '#suffix' => '</div></div>',
        '#title' => '',
    );

    $stepmenuarray = array(
        1 => 'TICKETS',
        2 => 'ENHANCEMENTS',
        3 => 'PAYMENT',
        4 => 'REVIEW ORDER',
        5 => 'CONFIRMATION'
    );
    
    $ticketsmenu = '<div class="menuRelative"><div class="subMenu responsiveWeb"><ul>';
    foreach ($stepmenuarray as $menukey => $menuvalue) {
        if ($menukey == $current_step) {
            $ticketsmenu.='<li class="active">' . $menuvalue . '</li>';
        } else {
            $ticketsmenu.='<li>' . $menuvalue . '</li>';
        }
    }
    $ticketsmenu .= '</ul></div><div class="responsiveMobile groupmarginTop" align="center"><select class="ticketpriceqty" disabled>';
    foreach ($stepmenuarray as $menukey => $menuvalue) {
        if ($menukey == $current_step) {
            $ticketsmenu.='<option selected="select">' . $menuvalue . '</option>';
        } else {
            $ticketsmenu.='<option>' . $menuvalue . '</option>';
        }
    }
    $ticketsmenu .= '</select></div></div>';


    $form['header']['stepmenu'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div class="row-fluid"><div class="span12">',
        '#suffix' => '</div></div>',
        '#title' => '',
        '#value' => $ticketsmenu
    );
    
    
	if ($current_step == 1) {
		$stages_content = '<div class="row-fluid shreBtnmargin responsiveWeb">
    					   <div class="span12"></div>
    					   </div>
    					   <div class="row-fluid responsiveWeb">
    					   <div class="span12">
    					   <div class="ticketTitle">PURCHASE TICKETS</div>
     					   </div>
     					   </div>';
	}



    if ($current_step == 2) {
        $stages_content = '<div class="row-fluid shreBtnmargin responsiveWeb">
    					   <div class="span12"></div>
    					   </div>
						   <div class="row-fluid responsiveWeb">
						   <div class="span12">
						   <div class="ticketTitle">ENHANCEMENTS</div>
						   </div>
						   </div>';
    }

    
    if ($current_step == 3) {
        $stages_content = '<div class="row-fluid shreBtnmargin responsiveWeb">
						   <div class="span12"></div>
						   </div>
						   <div class="row-fluid responsiveWeb">
						   <div class="span12">
						   <div class="ticketTitle">PAYMENT</div>
						   </div>
						   </div>';
    }
    
    
    if ($current_step == 4) {
        $stages_content = '<div class="row-fluid shreBtnmargin responsiveWeb">
						    <div class="span12"></div>
						    </div>
						    <div class="row-fluid responsiveWeb">
						    <div class="span12">
						    <div class="ticketTitle">Review Order</div>
						    </div>
						    </div>
						     
							<div class="row-fluid">
							<div class="span7">
							<div class="purchaseSubTitle">
						        		
							<a href="#" id="addvoucherlink">Add Promo code or Gift Card </a>
							</div>
							</div>
							<div class="span5"></div>
							</div>';
    }


    $form['header']['content'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div class="headercontent">',
        '#suffix' => '</div>',
        '#title' => '',
        '#value' => $stages_content,
    );

    return $form;
}


//Given the current stage the user is on, calculate what the next step would be
function booking_move_to_next_stage($form, &$form_state) {

    switch ($form_state['stage']) {
        case 'TICKETS':
            $_SESSION['formstage']="ENHANCEMENTS";
            return 'ENHANCEMENTS';
            break;

        case 'ENHANCEMENTS':
            $_SESSION['formstage']="PAYMENT";
            return 'PAYMENT';
            break;

        case 'PAYMENT':
            $_SESSION['formstage']="REVIEW_ORDER";
            return 'REVIEW_ORDER';
            break;
    }
}



//Given the current stage the user is on, calculate what the previous step would be
function booking_move_to_previous_stage($form, &$form_state) {
	
    switch ($form_state['stage']) {
        case 'ENHANCEMENTS':
            $_SESSION['formstage']="TICKETS";
            return 'TICKETS';
            break;

        case 'PAYMENT':
            $_SESSION['formstage']="ENHANCEMENTS";
            return 'ENHANCEMENTS';
            break;

        case 'REVIEW_ORDER':
            $_SESSION['formstage']="PAYMENT";
            return 'PAYMENT';
            break;
    }
}




function loadProductattributeoptions($productid) {
    $productnode = node_load($productid);
    $produtdetails = array();
    $results = array();
    $produtdetails['title'] = $productnode->title;
    //getting the Product Attributtes and options
    //make sure that accroding to the requirement we have one attirbute with one option
    foreach ($productnode->attributes as $options) {
        foreach ($options as $optionkey => $optionvalue) {
            $produtdetails[$optionkey] = $optionvalue;
        }

        $results[] = $produtdetails;
    }
    return $results;
}



//load catalog Products
function loadCatalogProducts() {

    $resultsarray = array();
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'taxonomy_term');
    $query->entityCondition('bundle', 'catalog');
    $query->propertyCondition('name', 'Enhancements'); //change 2 to any vocabulary ID
    $query->execute();
    if (count($entities->ordered_results) > 0) {
        foreach ($entities->ordered_results as $termresults) {
            $children = taxonomy_get_children($termresults->entity_id);
            foreach ($children as $childvalue) {
                //loading the Products
                $produtclist[$childvalue->name] = catalogEnhancementCategoryProducts($childvalue->tid);
                $resultsarray[] = $produtclist;
                unset($produtclist);
            }
        }
    }
    return $resultsarray;
}




function catalogEnhancementCategoryProducts($catalogid) {
	$resultsetnodes=array();
	$totalresults=array();
	$enhancemnetsnodes=getenhancementlistby($catalogid);
	foreach ($enhancemnetsnodes as $ennodes)
	{
		$resultsetnodes['id']=$ennodes->id;
		$resultsetnodes['title']=$ennodes->title;
		$resultsetnodes['price']=$ennodes->price;
		$resultsetnodes['description']=$ennodes->description;
		$resultsetnodes['caption']=$ennodes->caption;
		$resultsetnodes['hoursoffsale']=$ennodes->hoursoffsale;
		$resultsetnodes['nid']=$ennodes->nid;
		$resultsetnodes['status']=$ennodes->status;
		$resultsetnodes['categoryid']=$ennodes->categoryid;
		$resultsetnodes['created']=$ennodes->created;
		$resultsetnodes['enhancements_img_fid']=$ennodes->enhancements_img_fid;
		$totalresults[]=$resultsetnodes;
		unset($resultsetnodes);
	}
	
return $totalresults;
	
}


function catalogEnhancementCategoryProductsold($catalogid) {
    $resultsarray = array();
    $enhancementproducts = array();
    $query = new EntityFieldQuery();
    $entities = $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'product');
    $query->propertyCondition('status', 1); // Don't include unpublished products.
    $query->fieldCondition('taxonomy_catalog', 'tid', $catalogid);
    $query->execute();

    if (count($entities->ordered_results) > 0) {
        foreach ($entities->ordered_results as $productnodes) {
          //loading Products using entity id
		  //getenhancementlist
        	
            $productnodes = node_load($productnodes->entity_id);
            $enhancementproducts['nid'] = $productnodes->nid;
            $enhancementproducts['title'] = $productnodes->title;
            $enhancementproducts['status'] = $productnodes->status;
            $enhancementproducts['model'] = $productnodes->model;
            $enhancementproducts['price'] = $productnodes->price;
            $enhancementproducts['imageurl'] = file_create_url(getArrayloop($productnodes->uc_product_image));

            $resultsarray[] = $enhancementproducts;
            unset($enhancementproducts);
        }
    }

    return $resultsarray;
}





function buildform() {
    //Booking Flow Implementation
    $form = array();
    $form['ENHANCEMENTS'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div class="row-fluid"><div class="span6">',
        '#suffix' => '</div></div>',
        '#title' => '',
    );
    $result = loadCatalogProducts();

    foreach ($result as $productvalue) {
        foreach ($productvalue as $key => $productdetails) {
            $form['ENHANCEMENTS'][$key] = array(
                '#type' => 'fieldset',
                '#title' => t($key),
                '#weight' => 1,
                '#collapsible' => TRUE,
                '#collapsed' => FALSE,
                '#tree' => TRUE,
            );
            foreach ($productdetails as $productform) {
                $thumbhtml = '<div class="span6 blogInner">
    						  <div class="blogTitle">
   							  <span>' . $productform['name'] . '</span></div><img src="' . $productform['imageurl'] . '"></div>';
                
                $form['ENHANCEMENTS'][$key][$productform['name']] = array(
                    '#type' => 'fieldset',
                    '#prefix' => '<div class="headercontent">',
                    '#suffix' => '</div>',
                    '#title' => '',
                    '#value' => $thumbhtml,
                );
                
                //option set one field
                $form['ENHANCEMENTS'][$key][$productform['name']]['item-' . $productform['name']['nid']] = array(
                    '#type' => 'select',
                    '#title' => $productform['price'], //later we need to change this one as dynamic
                    '#options' => array(
                        0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7
                    ),
                    '#default_value' => 0,
                );
            }
        }
    }
    return $form;
}




//format:array(productenhancements=>array(enhancmentid,enhancementid,enhancmentid));
function getProductEnchancements($porudtctId) {
	$query = db_select('cruise_template_enhancements','cen');
	$query->fields('cen');
	$query->condition('cen.cruisetemplateid',2,'=');
	$result = $query->execute();
	$enhancmentresults=array();
	foreach ($result as $val)
	{
		$enhancmentresults=unserialize($val->templateenhancements);
	}
	
	return $enhancmentresults;//array(id1,id2,id3)
}




class CruiseEventObject{
	public static $eventName='';
	public static $departuretime='';
	public static $returntime;
	public static $portid;
	
	public static $data=array();
	public static function getCruiseEventDetails(){
		//Getting Eventname
		$eventid=$_SESSION['bookingInput']->eventid;
		$scheduledEventObject=scheduledevents_load($eventid);
		if(!is_null($scheduledEventObject) && isset($scheduledEventObject)){
			$data['eventname']=$scheduledEventObject->name;
			$data['portid']=$scheduledEventObject->port_id;
			$data['departuretime']=$scheduledEventObject->departuretime;
			$data['returntime']=$scheduledEventObject->returntime;
			$data['boardingtime']=$scheduledEventObject->boardingtime;
			$eventTimeUnix = strtotime($_SESSION['bookingInput']->date);
			$eventdate = date("F d, Y", $eventTimeUnix);
			$data['eventdate']=$eventdate;
			
			//Port Details
			$portData = loadportById($data['portid']);
			$data['portname']=$portData['portname'];
		}
		return $data;
	}
	
	public static function getCruiseEventWidget(){
		
	}
}



function getPrintOrderWidget($type) {
	
	
	$inputbookingdata = $_SESSION['bookingInput'];
	$cruiseTemplatId=$inputbookingdata->templateid;
	$cruiseObject=cruise_load($cruiseTemplatId);
	$cruiseTitle=$cruiseObject->cruise_title;
	
	
	
	
	$cruisedetails=CruiseEventObject::getCruiseEventDetails();
	
// 	drupal_json_output($cruisedetails) ;
// 	exit();
	
	$prefixwidget = '<div class="ordersummaryBox">
			<div class="sliderBox">
			<div class="sliderBoxout">
			<div class="orderBoxInner">
			<!--<div class="Ordertitle"><span>Order Summary</span><br><span>' . $cruisedetails ['eventname'] . '<br><br>
			<span>Departure Port : ' . $cruisedetails ['portname'] . '</span><br><span>Departure Time ' . $cruisedetails ['departuretime'] . '</span>&nbsp;&nbsp;<span>Return ' . $cruisedetails ['returntime'] . '</span></div>
			<div>-->
			
			<div class="widgettitleHeader">
			<div class="span2">
			<div class="line"></div>
			</div>
			<div class="span8" align="center">
			<h2>Order Summary</h2>
			</div>
			<div class="span2">
			<div class="line"></div>
			</div>
			</div>
			
			<div class="widgetBody">
			<h2>' .$cruiseTitle . '</h2>
			<h3>' . $cruisedetails ['eventdate'] . '</h3>
			<h4>Departure Port: ' . $cruisedetails ['portname'] . '</h4>
			
			<div class="timingDetails">
			<h3>Depart ' . $cruisedetails ['departuretime'] . '</h3>
			<h4><img src="' . base_path () . 'sites/all/themes/waterways/assets/img/dot.png"></h4>
			<h3>Return ' . $cruisedetails ['returntime'] . '</h3></div>';
	
	$suffixwidget='<div align="center"><img src="' . base_path () . 'sites/all/themes/waterways/assets/img/innerbox-widget-img.png"></div>
			</div>
			</div>
			</div>
			</div>';
	
	
	
	switch ($type) {
        case 'TICKETS' :
        	$ticketswidget = ticketswidget();
        	$enhancementorderwidget = enhancementWidgets ();
        	$orderlinewidget = orderlineitemwidgets ();
        	$printhtmlwidget = $prefixwidget . $ticketswidget .$enhancementorderwidget. $orderlinewidget . '
			</div>'.$suffixwidget;
        	return $printhtmlwidget;
        	break;
            break;

        case 'ENHANCEMENTS' :
        	$ticketswidget = ticketswidget();
        	$enhancementorderwidget = enhancementWidgets ();
        	$orderlinewidget = orderlineitemwidgets ();
        	$printhtmlwidget = $prefixwidget . $ticketswidget .$enhancementorderwidget. $orderlinewidget . '
			</div>'.$suffixwidget;
			return $printhtmlwidget;
			break;
		
		case 'PAYMENTS' :
			$ticketswidget = ticketswidget ();
			$enhancementorderwidget = enhancementWidgets ();
			$orderlinewidget = orderlineitemwidgets ();
			$printhtmlwidget = $prefixwidget . $ticketswidget . $enhancementorderwidget . $orderlinewidget . '
			</div>
			<br/>'.$suffixwidget;
			return $printhtmlwidget;
			break;
		
		case 'REVIEW' :
			$ticketswidget = ticketswidget ();
			$enhancementorderwidget = enhancementWidgets ();
			$orderlinewidget = orderlineitemwidgets ();
			$printhtmlwidget = $prefixwidget . $ticketswidget . $enhancementorderwidget . $orderlinewidget . '
			</div>
			<br/>'.$suffixwidget;
			return $printhtmlwidget;
			break;
		
		default :
			break;
	}
}


/* ->Cruise Fee Items
 *   Steps Caluclating the Cruise Fees
 *    1. Loading all the fee items where status = 1
 * 	  2. Loop all the items
 *    2.1 Loop all the tickets
 *    2.2 Fetch Ticket based on Ticket ID get the Fee ID
 *    2.3 if the FEE ID exists calcualte the amount
 *   
 * ->Coupon Code
 * ->Sub Total
 * ->Total Amount
 */
function orderlineitemwidgets() {
	$sessionid=$_SESSION['cruisecardsession'];
	$cruisefeehtml='';
	$promocodehtml='';
	$giftcardhtml='';
	$vouchercodehtml='';
	$subtotalhtml='';
	$totalhtml='';
	$taxamthtml='';
	
	
	
	//Cruise Fee Line Items
	
	$totalCruisefeeAmt=0;
	$chargeamt=0;
	$cruisefeehtml='';
	
	if(CartSession::getCruiseFeeItems($sessionid)){
		$cruiseFeeItems=CartSession::getCruiseFeeItems($sessionid);
		foreach($cruiseFeeItems as $item){
			$totalCruisefeeAmt+=$chargeamt;
			$chargeamt=$item['totalamt'];
			if($chargeamt>0){
				$cruisefeehtml.='<div class="taxCharges">';
				$cruisefeehtml.='<div class="chargesWrapper">';
				$cruisefeehtml.='<h3 class="chargesWrapperLeft">' . $item['title'] . '</h3>';
				$cruisefeehtml.='<span></span><h3 class="chargesWrapperRight">$' . wwcFormatPrice($chargeamt) . '</h3>';
				$cruisefeehtml.='</div></div>';
			}
		
		}
	}
	
	
	
	
	
    //Promocode
    $promocodeserialized=CartSession::getPromocode($sessionid);
    $promocodediscount=0;
    if($promocodeserialized){
    	$promocode='';
    	$promocodeArr=unserialize($promocodeserialized);
    	foreach($promocodeArr as $key=>$value){
    		$promocode=$key;
    		$promocodediscount=$value;
    	}
    }
    $cpndivstatus=($promocodeserialized) ? "block" : "none";
    $promocodehtml.='<div id="cpnmaindiv" style="display:'.$cpndivstatus.';" class="taxCharges">';
    $promocodehtml.='<div class="chargesWrapper">';
    $promocodehtml.='<h3 class="chargesWrapperLeft">Promo Code</h3>';
    $promocodehtml.='<span></span><h3 class="chargesWrapperRight"><div id="cpnamountdiv">-$' . wwcFormatPrice($promocodediscount).'</div></h3>';
    $promocodehtml.='</div></div>';
    
 
    
    //Gift Card
    $giftCardserialized=CartSession::getGiftCard($sessionid);
    $giftCardAmount='';
    if($giftCardserialized){
    	$giftCardArray=unserialize($giftCardserialized);
    	$giftCardCode='';
    	
    	foreach($giftCardArray as $key=>$values){
    		$giftCardCode=$key;
    		$giftCardAmount=$giftCardArray[$key]['deductedamount'];
    	}
    }
    $gifctcarddivstatus=($giftCardserialized) ? "block" : "none";
    $giftcardhtml.='<div id="giftcardmaindiv" style="display:'.$gifctcarddivstatus.';" class="taxCharges">';
    $giftcardhtml.='<div class="chargesWrapper">';
    $giftcardhtml.='<h3 class="chargesWrapperLeft">Gift Card</h3>';
    $giftcardhtml.='<span></span><h3 class="chargesWrapperRight"><div id="giftcardamountdiv">-$' . wwcFormatPrice($giftCardAmount).'</div></h3>';
    $giftcardhtml.='</div></div>';
    
    
    
    //Voucher codes
    $voucherAmount=CartSession::getVoucherCodesPrice($sessionid);
    //if($voucherAmount!="" || $voucherAmount!=0){
    	$vouchermaindiv=($voucherAmount>0) ? "block" : "none";
    	$vouchercodehtml.='<div id="vouchermaindiv" style="display:'.$vouchermaindiv.';" class="taxCharges">';
    	$vouchercodehtml.='<div class="chargesWrapper">';
    	$vouchercodehtml.='<h3 class="chargesWrapperLeft">Voucher</h3>';
    	$vouchercodehtml.='<span></span><h3 class="chargesWrapperRight"><div id="voucheramtdiv">-$' .wwcFormatPrice($voucherAmount).'</div></h3>';
    	$vouchercodehtml.='</div></div>';
    //}
   
    
    //Sub Total
    $orderSubTotal=CartSession::getOrderSubTotal($sessionid);
    if($orderSubTotal!=""){
    	$subtotalhtml = '';
    	$subtotalhtml.='<div class="taxCharges">';
    	$subtotalhtml.='<div class="chargesWrapper">';
    	$subtotalhtml.='<h3 class="chargesWrapperLeft">Subtotal</h3>';
    	$subtotalhtml.='<span></span><h3 class="chargesWrapperRight">$' . wwcFormatPrice($orderSubTotal) . '</h3>';
    	$subtotalhtml.='</div>';
    }
    
    
    //Tax
    $taxAmt=CartSession::getTaxAmt($sessionid);
    if($taxAmt!=""){
    	$taxamthtml = '';
    	$taxamthtml.='<div class="taxCharges">';
    	$taxamthtml.='<div class="chargesWrapper">';
    	$taxamthtml.='<h3 class="chargesWrapperLeft">Tax</h3>';
    	$taxamthtml.='<span></span><h3 class="chargesWrapperRight">$' . wwcFormatPrice($taxAmt) . '</h3>';
    	$taxamthtml.='</div>';
    }
    
    
    
    
    //************** Total Amount ****************************
    $orderTotal=CartSession::getOrderTotal($sessionid);
   if($orderTotal!=""){
    	$totalhtml.='<div class="totalDetails">';
    	$totalhtml.='<h2 align="left">Total</h2>';
    	$totalhtml.='<h2 align="right"><div id="totalamtdiv">$' .wwcFormatPrice($orderTotal).'</div></h2>';
    	$totalhtml.='</div>';
    	if($_SESSION['formstage']=="PAYMENT"){
    		$totalhtml.="<a href='#' id='editticketorderlinkPayment'>EDIT TICKET ORDER </a><br/>";
    		$totalhtml.="<a href='#' id='backtoenhancementslink'>BACK TO ENHANCEMENTS</a><br/>";
    	}
    	if($_SESSION['formstage']=="ENHANCEMENTS"){
    		$totalhtml.="<a href='#' id='editticketorderlinkEnhancements'>EDIT TICKET ORDER </a><br/>";
    	
    	}
   }
    
   $html=$cruisefeehtml.''.$promocodehtml.''.$giftcardhtml.''.$vouchercodehtml.''.$subtotalhtml.''.$taxamthtml.''.$totalhtml;
   //$ordertotals = $subchargeswidget . $taxsubchargeswidget . $couponwidgethtml . $voucherwidgethtml . $giftcardwidgethtml . $subtotalwidgethtml . $totalwidgethtml;
    return $html;
}




/*
 * Enhancement Widget
 * 
 */

function enhancementWidgets() {
	$enhancementhtml='';
	$sessionid=$_SESSION['cruisecardsession'];
	
	if(CartSession::getEnhancements($sessionid)){
		$enhancmentsitems=unserialize(CartSession::getEnhancements($sessionid));
		
		foreach ($enhancmentsitems as $enhancmentwidget) {
			$amount = $enhancmentwidget['qty'] * $enhancmentwidget['price'];
			$enhancementhtml.='<div class="ticketDetails">';
			$enhancementhtml.='<h3 align="left">Enhancement <br/><span >' . $enhancmentwidget['qty'] . ' ' . $enhancmentwidget['name'] . '</span></h3>';
			$enhancementhtml.='<h3 align="right">Price <br/><span>$' . wwcFormatPrice($amount).'</span></h3>';
			$enhancementhtml.='</div>';
			$enhancementhtml.='<div class="line"></div><div class="line"></div>';
		}
	}
	
    return $enhancementhtml;
}

/*
 * Ticket Wiget
 * 
 */
function ticketswidget() {
	$tickethtml='';
	$sessionid = $_SESSION ['cruisecardsession'];
	
	if(CartSession::getTickets ( $sessionid )){
		$tickets = unserialize(CartSession::getTickets ( $sessionid ));
		
		
		foreach ( $tickets as $ticket ) {
			if ($ticket ['qty'] > 0) {
				$tickethtml .= '<div class="ticketDetails">';
				$tickethtml .= '<h3 align="left">Ticket <br/><span >' . $ticket['qty'] . ' ' . $ticket['ticketname'] . '</span></h3>';
				$amount = $ticket['qty'] * $ticket['price'];
				$tickethtml .= '<h3 align="right">Price <br/><span>$' . wwcFormatPrice($amount) . ' </span></h3>';
				$tickethtml .= '</div>';
				$tickethtml .= '<div class="line"></div><div class="line"></div>';
			}
		}
	}
	
	
	return $tickethtml;
}







/*
 * old code
 * 
 * function buildTicketwidget() {
    $enhancementorderwidget = buildEnhancementOrderwidget($_SESSION['cruisecardsession']);
//     var_dump($enhancementorderwidget);
//     exit();
    
//    return "hello";
//      drupal_json_output($enhancementorderwidget);
//      exit();
//    $tickethtml = '';
    foreach ($enhancementorderwidget as $ticketswidgets) {
        $tickethtml.='<div class="ticketDetails">';
        $tickethtml.='<h3 align="left">Ticket <br/><span >' . $ticketswidgets->qty . ' ' . $ticketswidgets->name . '</span></h3>';

        $amount = $ticketswidgets->qty * $ticketswidgets->price;
        $amountFormattedString = number_format($amount, 2);
        $amtArray = explode('.', $amountFormattedString);
        $amtWholeNumber = 0;
        $amtDecimalNumber = 0;
        if (isset($amtArray)) {
            $amtWholeNumber = $amtArray[0];
            $amtDecimalNumber = $amtArray[1];
        }
        $amt = '$' . $amtWholeNumber . '.<sup>' . $amtDecimalNumber . '</sup>';


        //$tickethtml.='<h3 align="right">Price <br/><span>' . uc_currency_format($ticketswidgets->qty * $ticketswidgets->price) . '<sup>00</sup></span></h3>';
        $tickethtml.='<h3 align="right">Price <br/><span>' . $amt . ' </span></h3>';
        $tickethtml.='</div>';
        $tickethtml.='<div class="line"></div><div class="line"></div>';
    }
    return $tickethtml;
}




function buildEnhancementOrderwidget($sessionId) {
	
  
    $loadattribes = getCartSessionItemsbysessionId($sessionId);
    

    
    
//    var_dump($loadattribes);
//     exit();
    
    foreach($loadattribes as $attribs){
    	$ticketobject=new stdClass();
    	$ticketobject->ticketid =$attribs['ticketid'];
    	$ticketobject->qty=$attribs['qty'];
    	$ticketobject->price =$attribs['price'];
    	
    	$query = db_select('cruisetickets', 'c');
		$query->fields('c', array('title'));
		$query->condition('c.id',$attribs['ticketid']);
		$result = $query->execute()->fetchObject();
		$ticketobject->name=$result->title;
    	
    	
    	
    	$productoptions[]=$ticketobject;
    	//$productoptions[] = $ucopotions;
    }
    
   
    
   return $productoptions;
    
    $productoptions = array();
    foreach ($loadattribes as $attribs) {
//          drupal_json_output($attribs);
//          exit();
         //$t= uc_attribute_option_load('18');
        // drupal_json_output($t);
   // exit();
        //load options

        $ucopotions = (uc_attribute_option_load((int) $attribs['optionid']));
        //Product variation price
        $loadprodcuoptionalprice = db_query("SELECT * FROM {uc_product_options} WHERE oid = :oid AND nid=:nid", array(':oid' => (int) $attribs['optionid'], ':nid' => $_SESSION['bookingInput']->eventid))->fetchObject();
        $itemkey = $attribs['optionid'] . '-' . 'price';
        $ucopotions->qty = $attribs['qty'];
        $ucopotions->price = $loadprodcuoptionalprice->price;
        $productoptions[] = $ucopotions;
    }
    
   
    var_dump($productoptions);
    exit();
    
//    drupal_json_output($productoptions);
//    exit();
    return $productoptions;
}

function getProductEnchancementsold($porudtctId) {
    $result = db_query('SELECT *
FROM {uc_product_enhancements} pe WHERE pe.nid = :nid', array(':nid' => $porudtctId));
    foreach ($result as $value) {
//  $response['bookitems']=json_decode(unserialize($value->bookitems));

        $response['nid'] = $value->nid;
        $response['productenhancementids'] = (array) (unserialize($value->productenhancements));
    }
    return $response['productenhancementids']['enhancedproducts'];//array(id1,id2,id3)
}






 */