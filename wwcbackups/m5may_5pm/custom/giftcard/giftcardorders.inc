<?php
function giftcardorders_page(){
	
	
$header = array (
			array ('data' => 'OrderId',
			),
			array ('data' => 'Customer',
			),
			array ('data' => 'Email',
			),
			array ('data' => 'Phone' 
			),
			array ('data' => 'Purchase date' 
			),
			array ('data' => 'Total Amount' 
			),
			array ('data' => 'Status' 
			),
			array ('data' => 'Action' 
			) 
	);

	

	$sort = 'DESC';
	$query = db_select('uc_orders', '');
	$query->fields('giftcards_order_tracking', array('order_id', 'uid', 'giftcard_qty', 'giftcard_totalprice'));
	$query->fields('uc_orders');
	$query->innerJoin('giftcards_order_tracking', '', 'uc_orders.order_id = giftcards_order_tracking.order_id');
	$query->orderBy('giftcards_order_tracking.order_id', $sort);
	$query = $query->extend('TableSort')->extend('PagerDefault')->limit(10);
	$result = $query->execute()->fetchAll();
	
	
	global $base_url;
	$rows = array();
	if (count($result) > 0) {
		for ($i = 0; $i < count($result); $i++) {
			$access = $result[$i]->access ? t('@time ago', array('@time' => format_interval(REQUEST_TIME - $result[$i]->access))) : t('never');
			$status = array(t('blocked'), t('active'));
			$orderstatus = str_replace('_', ' ', $result [$i]->order_status);
			$rows[] = array(
					$result[$i]->order_id,
					$result [$i]->delivery_last_name . ' ' . $result [$i]->delivery_first_name,
					$result [$i]->primary_email,
                $result [$i]->delivery_phone,
                date('m-d-Y', $result [$i]->created),
                uc_currency_format($result [$i]->order_total),
                ucwords($orderstatus),
				'<a href="' . $base_url . '/giftcardorder/' . $result[$i]->order_id . '/view">View </a>'
			);
		}
	}
	
	$output .= '<div class="">';
	$output .= '<div class="">';
	
	$output .= '</div>';
	$output .= theme_table(
			array(
					'header' => $header,
					'rows' => $rows,
					'attributes' => array('class' => array('')),
					'sticky' => true,
					'caption' => '',
					'colgroups' => array(),
					'empty' => t("No Records!"),
			)
	) . theme('pager');
	
	$output .= '</div>';
	
	$form['mkp'] = array(
			'#type' => 'markup',
			'#markup' => $output,
	);
	
	return $form;
	
	
	
	
	drupal_json_output($result);
	exit();
	return "hello";
}