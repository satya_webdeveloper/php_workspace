<?php


function giftcard_form_submit($form, &$form_state) {
	
	//drupal_json_output($form_state['multistep_values']); exit();
	switch ($form_state ['stage']) {
		case 'review_details' :
			$form_state ['multistep_values'] [$form_state ['stage']] = $form_state ['values'];
			if ($form_state ['triggering_element'] ['#value'] != 'Back') {
				giftcard_credit_card_details_submit ( $form, $form_state );
				$form_state ['new_stage'] = giftcard_move_to_next_stage ( $form, $form_state );
			}
			break;
		
		default :
			$form_state ['multistep_values'] [$form_state ['stage']] = $form_state ['values'];
			$form_state ['new_stage'] = giftcard_move_to_next_stage ( $form, $form_state );
			break;
	}
	
	if ($form_state ['triggering_element'] ['#value'] == 'Back') {
		$form_state ['new_stage'] = giftcard_move_to_previous_stage ( $form, $form_state );
	}
	
	if (isset ( $form_state ['multistep_values'] ['form_build_id'] )) {
		$form_state ['values'] ['form_build_id'] = $form_state ['multistep_values'] ['form_build_id'];
	}
	$form_state ['multistep_values'] ['form_build_id'] = $form_state ['values'] ['form_build_id'];
	$form_state ['stage'] = $form_state ['new_stage'];
	
	$form_state ['rebuild'] = TRUE;
}





/**
 * Handles the submission of the final stage
 */
/*
 * Satya
 * 
 * ->Mailing Address Details
 * ->Credit Card Details
 * ->Review Details
 * ->Gift Card Details
 * 
 * ->Ubercart Product
 *   To Do Right Now the The Giftcard product ID is hardcoded
 *   here product 272 is hardcododed it is not good practice .................
 *   Issue : if product 272 is deleted
 *   
 * ->Gift Card Price (may vary)
 * 
 * ->Creating User Account
 * 
 * ->Ubercart Order
 *   Creating Order Prograamatically
 *   
 * ->Logging Gift Card Order
 * 
 * ->Adding Product to Ubercart Order
 */
function giftcard_credit_card_details_submit($form, &$form_state) {
	$gift_card_details_values = isset($form_state['multistep_values']['gift_card_details']) ? $form_state['multistep_values']['gift_card_details'] : array();

	
	$giftcardprice=0;
	$userId=false;
	$productqty=0;
	
	$mailingAddressData=$form_state['multistep_values']['mailing_address_details'];
	$giftCardData=$form_state['multistep_values']['gift_card_details'];
	$creditCardData=$form_state['multistep_values']['credit_card_details'];
	$reviewData=$form_state['multistep_values']['review_details'];
	
	
	
	
	//User Account
	$userObject = user_load_by_mail($mailingAddressData['customeremail']);
	$userId=false;
	if ($userObject) {
		$userId = $userObject->uid;
	} else {
		$userObject = new UserAccount ();
		$userObject->user_name = $mailingAddressData['customerfirstname'].''.$mailingAddressData['customerlastname']. time();
		$userObject->password = user_password ( 6 );
		$userObject->firstname = $mailingAddressData['customerfirstname'];
		$userObject->last_name = $mailingAddressData['customerlastname'];
		$userObject->mail = $mailingAddressData['customeremail'];
		$userObject->role_id = 7;
		$userObject->phone = $mailingAddressData['customerphonenumber'];
		$userObject->address = $mailingAddressData['customeraddress'] . " " . $mailingAddressData['customercity'] . " " . $mailingAddressData['customerzipcode'];
		$userId = $userObject->createNewUser();
	}
	
	
	
	
	
	
	//Product
	$giftproductdata = node_load(272); //load the node of content type product.
	$giftcardAmt=$giftCardData['pregiftcardamount'];
	if(strcasecmp($giftCardData['pregiftcardamount'],"setmyown")==0){
		$giftcardAmt=$giftCardData['customgiftcardamount'];
	}

	$productqty = trim($giftCardData["giftcardqty"]);
	
	
	//Ubercart Order
	$order = uc_order_new($userId, 'in_checkout'); //Creating the new Order
	$order_id = $order->order_id;
	

	
	$orderData=new stdClass();
	$orderData->orderid=$order_id;
	$orderData->data=serialize($form_state['multistep_values']);
	GiftCard::loggingGiftCardOrderData($orderData);
	
	//Loading Billing Information(billing Details)
	$billingvalues = isset($form_state['multistep_values']['mailing_address_details']) ? $form_state['multistep_values']['mailing_address_details'] : array();
	$order->billing_first_name = $mailingAddressData['customerfirstname'];
	$order->billing_last_name = $mailingAddressData['customerlastname'];
	$order->billing_company = null;
	$order->billing_street1 = $mailingAddressData['customeraddress'];
	$order->billing_postal_code = $mailingAddressData['customerzipcode'];
	$order->billing_city = $mailingAddressData['customercity'];
	$order->billing_zone = $mailingAddressData['customerstate'];
	$order->billing_country = 840;
	$order->billing_phone = $mailingAddressData['customerphonenumber'];
	
	$order->primary_email = $mailingAddressData['customeremail'];
	
	//Loading Shipping information
	$order->delivery_first_name = $mailingAddressData['customerfirstname'];
	$order->delivery_last_name = $mailingAddressData['customerlastname'];
	$order->delivery_company = null;
	$order->delivery_street1 = $mailingAddressData['customeraddress'];
	$order->delivery_postal_code = $mailingAddressData['customerzipcode'];
	$order->delivery_city = $mailingAddressData['customercity'];
	$order->delivery_zone = $mailingAddressData['customerstate'];
	$order->delivery_country = 840;
	$order->delivery_phone = $mailingAddressData['customerphonenumber'];
	

	//Adding Product to Ubercart Order
	
	
	
	$giftProduct = new stdClass ();
	$giftProduct->nid = $giftproductdata->nid;
	$giftProduct->title = $giftproductdata->title;
	$giftProduct->model = $giftproductdata->model;
	$giftProduct->qty = $productqty;
	$giftcardprice = sprintf ( '%.4f', $giftcardAmt );
	$giftProduct->price = $giftcardAmt;
	$order->products [] = $giftProduct;
	uc_order_save($order);
	

	//->Logging Gift Card Order
	$giftCardOrderObject= new GiftCardOrder();
	$giftCardOrderObject->giftcard_price=$giftcardprice;
	$giftCardOrderObject->giftcard_totalprice=$giftProduct->price ;
	$giftCardOrderObject->giftcard_qty=$productqty;
	$giftCardOrderObject->order_id=$order->order_id;
	$giftCardOrderObject->uid=$userId;
	$giftCardOrderObject->created=REQUEST_TIME;
	GiftCard::loggingGiftCardOrder($giftCardOrderObject);

    
    $order->uid = $userId;
    $currenttotal = uc_order_get_total($order);
  	//Loading the Payment Creditcard details information
    $order->payment_method = 'credit';
    $order->payment_details['cc_number'] = $creditCardData['creditcardnumber']; 
    $order->payment_details['cc_exp_month'] = $creditCardData['cardexpmonth']; 
    $order->payment_details['cc_exp_year'] = $creditCardData['cardexpyear']; 
    $order->payment_details['cc_cvv'] = $creditCardData['creditcardcvv']; 
    $order->payment_details['cc_type'] = $creditCardData['creditcardtype'];
    //save the Order Object using order_save 
    
    
    try{
    	$orderobj=new stdClass();
    	$orderobj->ccnum=$creditCardData['creditcardnumber'];
    	$orderobj->cvv="";
    	$orderobj->cctype=$creditCardData['creditcardtype'];
    	$orderobj->ccexpirationdate=$creditCardData['cardexpmonth']."".$creditCardData['cardexpyear'];
    	$orderobj->order_id=$order->order_id;
    	CreditCardFreezer::order_ccdata_create($orderobj);
    
    }catch(Exception $e){
    
    }
    
    uc_order_save($order);
  
 

   	//Updating the Payment Method on After Ordercreation completed in 'in-checkout mode
    $order = uc_order_load($order->order_id);
    db_update('uc_orders')
            ->fields(array('payment_method' => $order->payment_method))
            ->condition('order_id', $order->order_id)
            ->execute();

    //set the Payment Type
    $data = array();
    //validation
    $paymentstatus=false;
    $module = 'giftcard';
    $key = 'giftcard_incomplete';
   
    $_SESSION['giftcardpurchasestatus']=false;
    
    $availalegiftcardsqty=GiftCard::checkAvaialbleGiftCardsQuantity();
     if ($availalegiftcardsqty > 0) {
    	if ($productqty<=$availalegiftcardsqty) {
    		//watchdog('current amt entred', $order->order_total);
    		$data['txn_type'] = UC_CREDIT_AUTH_CAPTURE;
    		    		
    		//Executing the Payment Process
    		$paymentgiftcard = uc_payment_process_payment('credit', $order->order_id, $order->order_total, $data, TRUE, NULL, FALSE);
    		_uc_credit_save_cc_data_to_order($order->payment_details, $order->order_id);
    	
    		if ($paymentgiftcard) {
    			
    			
    			$_SESSION['giftcardpurchasestatus']=true;
    			
    			//update the stock information
//     			$ordersesult = getStockUpdateByOrder($order);
//     			if ($ordersesult) {
//     				watchdog('giftcardpurchase', 'Updated sucessfully for ' . $order->order_id);
//     			} else {
//     				watchdog('giftcardpurchase', 'Updation failed for ' . $order->order_id);
//     			}
    			//end of stock updation
    			 
    			 
    			//creating Gift object.
    			$_SESSION['currentgiftcard_orderid']=$order->order_id;
    			$giftinputobj = new stdClass();
    			$giftinputobj->orderid=$order->order_id;
    			$giftinputobj->uid=$userId;
    			$giftinputobj->giftcarddata = $giftCardData;
    			$giftinputobj->mailingaddressdata = $mailingAddressData;
    			$giftinputobj->creditdata = $creditCardData;
    			$giftinputobj->reviewdata = $reviewData;
    			
    			
    			
    			
    			
    			 
    			//callin the profit point API for gift Issuance.
    			$resultset = profitpointapi_giftissuance($giftinputobj);
    			
    			
    			//Sending Email
    			
    			//Loading the Gift Cards from DB
    		    $orderedgiftcardsstr='';			
    			$giftcards=GiftCardsTrack::getCardsByOrderId($order->order_id);
    			$orderedcards=array();
    			foreach($giftcards as $giftcard){
    				$orderedcards[]=$giftcard->card_id;
    			}
    			
    			if(count($orderedcards)>0){
    				$orderedgiftcardsstr=implode(',', $orderedcards);
    			}
    			
    			$params = array(
    					'subject' => t('A gift for you!'),
    					'r_name' => $giftCardData['recipientemail'],
    					's_name' => $giftCardData['yourname'],
    					'amt' => $giftcardAmt,
    					'body' => t("Data"),
    					'giftcardnumbers' => $orderedgiftcardsstr,
    					'message'=>$giftCardData['customermessage']
    			);
    			$to = $giftCardData['recipientemail'];
    			drupal_mail("email_system", "wwc_giftcard_mail", $to, language_default(), $params, "admin@mysite.com");

    			
    			$params = array(
    					'subject' => t('Your Order at WATERWAYS!'),
    					'r_name' => $giftCardData['recipientemail'],
    					's_name' => $giftCardData['yourname'],
    					'amt' => $giftcardAmt,
    					'body' => t("Data"),
    					'giftcardnumbers' => $orderedgiftcardsstr,
    					'order_id'=>$order->order_id,
    					'message'=>$giftCardData['customermessage']
    			);
    			$to = $order->primary_email;
    			drupal_mail("email_system", "wwc_giftcard_invoice_mail", $to, language_default(), $params, "admin@mysite.com");
    			 
    			$module = 'giftcard';
    			$key = 'giftcard_complete';
    		}
    	}else{
    	}
    }else{
    	//Logging
    	//drupal_set_message ( 'There are not enough Giftcards available for your request. Please call (206) 223-2060 to speak with a sales representative.' );
    	//do the payment here
    }
    
    
}

/**
 * Returns what to show on the completion page.
 * 
 * @return type 
 */
function giftcard_complete() {

    return 'Thank you for completing our survey. 
      You have been sent an email confirming you\'ve been entered into our prize draw';
}

function giftcard_details($productqty) {
    $query = db_select('giftcard', 'g');
    $query->condition('flag', 0);
    $query->fields('g', array('card_id'));
    $query->range(0, $productqty);
    $result = $query->execute()
            ->fetchAll();
    return $result;
}


//Satya : What ?
function giftcardupdate($cardid) {
    $query = db_update('giftcard');
    $query->fields(array('flag' => 1));
    $query->condition('card_id', $cardid, '=');
    $result = $query->execute();
}

//

