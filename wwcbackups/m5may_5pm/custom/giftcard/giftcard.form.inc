<?php

function giftcard_add() {

    $giftcard = (object) array(
                'id' => '',
                'card_id' => '',
                'flag' => '',
                'created' => '',
    );

    return drupal_get_form('giftcard_add_form', $giftcard);
}

function giftcard_add_form($form, &$form_state, $giftcard) {

    $formstatus = $giftcard->id == '' ? "add" : "edit";
    $form['operation'] = array(
        '#type' => 'hidden',
        '#value' => $formstatus
    );

    $auto_id = $giftcard->id != '' ? $giftcard->id : "";
    $form['auto_id'] = array(
        '#type' => 'hidden',
        '#value' => $auto_id
    );


    $form['cardid'] = array(
        '#type' => 'textfield',
        '#title' => t('Card ID'),
        '#default_value' => isset($giftcard->card_id) ? $giftcard->card_id : '',
        '#required' => TRUE,
    );
    field_attach_form('giftcard', $giftcard, $form, $form_state);
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
    );
    $form['cancel'] = array(
        '#type' => 'submit',
        '#value' => t('Cancel'),
        '#submit' => array('our_callback_for_cancel_button'),
        '#limit_validation_errors' => array(),
    );
    return $form;
}

function our_callback_for_cancel_button($form, &$form_state) {
    //check whether we have destination to redirect after clicking the cancel button otherwise redirect to front page
    $url = $link = $base_url . '/giftcard';
    drupal_goto($url);
}

function giftcard_add_form_validate($form, &$form_state) {
    $giftcard = (object) $form_state['values'];
    $card_id = $form_state['values']['cardid'];
    //var_dump($giftcard);
    //exit();

    $result = duplicate_card($card_id);
    if ($result > 0) {
        form_set_error('cardid', 'you have enter already exist card id');
    } elseif (!is_numeric($card_id)) {
        form_set_error('cardid', 'you have enter invalid card id');
    }
    //duplicate_card_check('')
    field_attach_form_validate('giftcard', $giftcard, $form, $form_state);
}

function giftcard_add_form_submit($form, &$form_state) {


    //Credit Card
    $card_id = $form_state['values']['cardid'];


    $auto_id = $form_state['values']['auto_id'];

    $giftcard = new stdClass();
    $giftcard->id = $auto_id;
    $giftcard->card_id = $card_id;
    $giftcard->created = time();
    $giftcard->flag = "0";
    field_attach_submit('giftcard', $giftcard, $form, $form_state);

    $giftcard = giftcard_save($giftcard);

    $operation = $form_state['values']['operation'];
    $msg = '';
    if ($operation == "add") {
        $msg = 'Gift Card Created Successfully.';
    } else {
        $msg = 'Gift Card Updated Successfully.';
    }

    drupal_set_message($msg);
    $form_state['redirect'] = 'giftcard';
}

//Giftcard BulkUpload
function giftcard_bulkupload_import_form($form, $form_state) {

    $form['giftcardsample'] = array(
        '#markup' => '<a href="' . base_path() . 'sites/default/files/tmp/giftcarddata.xls">Download Excel Here</a>',
    );
    $form['import'] = array(
        '#title' => t('Import'),
        '#type' => 'managed_file',
        '#description' => t('Upload a Excel File.'),
        '#upload_location' => 'public://tmp/',
        '#upload_validators' => array(
            'file_validate_extensions' => array('xls xlsx'),
        ),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Import'),
    );
    return $form;
}

function giftcard_bulkupload_import_form_submit($form, $form_state) {
    if ($form_state['values'] ['import'] != 0) {
        $excelFileName = db_query("SELECT uri FROM {file_managed} WHERE fid = :fid", array(
            ':fid' => $form_state['values'] ['import'],
                ))->fetchField();

        if (file_exists($excelFileName)) {


            $objPHPExcel = PHPExcel_IOFactory::load($excelFileName);
            $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $headingsArray = $objWorksheet->rangeToArray('A1:' . $highestColumn . '1', null, true, true, true);
            $headingsArray = $headingsArray[1];
            $r = -1;
            $excelUserDataArray = array();
            try {
                for ($row = 2; $row <= $highestRow; ++$row) {
                    $dataRow = $objWorksheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, true, true);
                    if ((isset($dataRow[$row]['A'])) && ($dataRow[$row]['A'] > '')) {
                        ++$r;
                        foreach ($headingsArray as $columnKey => $columnHeading) {
                            $excelUserDataArray[$r][$columnHeading] = $dataRow[$row][$columnKey];
                        }
                    }
                }
            } catch (Exception $e) {
                watchdog_exception('Reading Excel File Error', $e, NULL, WATCHDOG_ERROR);
                return FALSE;
            }

            $resultkey = TRUE;
            try {
                if (count($excelUserDataArray) > 0) {
                    $successgiftcards = array();
                    $failuregiftcards = array();
                    //Inserting Excel Records into Database Starts here
                    for ($i = 0; $i < count($excelUserDataArray); $i++) {
                        $cardid = number_format($excelUserDataArray[$i]["GiftcardId"], 0, '.', '');
                        $cardresult = array();
                        $cardresult = duplicate_card_check($cardid);
//Inserting giftcard detials
                        if (count($cardresult) > 0) {
                            $failuregiftcards[] = $cardid;
                        } else {
                            $result = db_insert('giftcard')
                                    ->fields(array(
                                        'card_id' => number_format($excelUserDataArray[$i]["GiftcardId"], 0, '.', ''),
                                        'created' => time()
                                    ))
                                    ->execute();
                            if ($result) {
                                $successgiftcards[] = $cardid;
                            } else {
                                $failuregiftcards[] = $cardid;
                            }
                        }
                    }


                    /*     here we need to create the excel file send to logged users regarding success giftcards and failure giftcards     */
                    $filename = rand('1111111', '9999999') . '.xls';
                    $attachfilename = $filename;
                    $directory_path = file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath() . '/giftcardexceluploads/';
                    $filename = $directory_path . $filename;
                    $sep = "\t";        //tabbed character
                    $fp = fopen($filename, 'w') or die("can't open file");

                    $schema_insert_rows = "";
                    //printing column names 

                    $schema_insert_rows.="#" . $sep;
                    $schema_insert_rows.="Giftcard ID" . $sep;
                    $schema_insert_rows.="Status" . $sep;
                    $schema_insert_rows.="\n";
                    fwrite($fp, $schema_insert_rows);

// printing data:
                    $row = 0;
                    $i = 0;

                    for ($i = 0; $i < count($successgiftcards); $i++) {
                        $schema_insert_rows = "";
                        $schema_insert_rows .= ( ++$row) . "" . $sep;
                        $schema_insert_rows .= $successgiftcards[$i] . "" . $sep;
                        $schema_insert_rows .= "Success";
                        $schema_insert_rows .= "\n";
                        fwrite($fp, $schema_insert_rows);
                    }

                    for ($i = 0; $i < count($failuregiftcards); $i++) {
                        $schema_insert_rows = "";
                        $schema_insert_rows .= ( ++$row) . "" . $sep;
                        $schema_insert_rows .= $failuregiftcards[$i] . "" . $sep;
                        $schema_insert_rows .= "Failed";
                        $schema_insert_rows .= "\n";
                        fwrite($fp, $schema_insert_rows);
                    }
                    fclose($fp);


                    $frommail = variable_get('site_mail');

                    /*     Here we build the email system for sending attachment     */
                    $attachmentfile = $directory_path;
                    giftcard_notify($frommail, $attachfilename);
                } else {
                    return FALSE;
                }
            } catch (Exception $e) {
                watchdog_exception('Inserting Records into Giftcard table Error', $e, NULL, WATCHDOG_ERROR);
                return FALSE;
            }
        }
    } else {
        $msg='Please Upload Excel File and Try Again.';
        drupal_set_message($msg, $type = 'error');

    }
            return $resultkey;
        }

        function giftcard_notify($frommail, $filename) {
            global $user;
            $tomail = $user->mail;
            $params['filename'] = $filename;
            // giftcard_mail()  will be called based on the first drupal_mail() parameter.
            drupal_mail('giftcard', 'notice', $tomail, $frommail, $params);

    drupal_set_message('Giftcards Inserted and not Inserted List has been emailed to your Email Id');
        }

        function giftcard_mail($key, &$message, $params) {
            global $user;

            $account = $user->name;
            $attachment = array(
                'filecontent' => file_get_contents(file_stream_wrapper_get_instance_by_uri('public://')->getDirectoryPath() . '/giftcardexceluploads/' . $params['filename']),
                'filename' => 'Gift-Cards Inserted and failed',
                'filemime' => 'application/xls',
            );

            switch ($key) {
                case 'notice':
                    $langcode = $message['language']->language;
                    $message['subject'] = 'Giftcards Data ' . $account;
                    $message['body'][] = 'Inserted and falied Gift cards';
                    $message['params']['attachments'][] = $attachment;
                    break;
            }
        }
