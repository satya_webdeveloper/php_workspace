<?php
function giftcard_order_view_page($order_id){
	
	return giftcardorderpagetemplate($order_id);
	
}



function giftcardorderpagetemplate($orderId) {
	
	$giftcardorderdata=GiftCard::getGiftCardOrderData($orderId);
	$userdata=unserialize($giftcardorderdata->data);
	$giftcarddetails=$userdata['gift_card_details'];
	
	//var_dump($giftcarddetails); exit();
	
	drupal_set_title("Gift Card Order : ".$orderId);
	global $base_url;
	$html = '';

		$order=uc_order_load($orderId);
        $r_name=$giftcarddetails['recipientemail'];
        
        $amt=$giftcarddetails['pregiftcardamount'];
        if(strcasecmp($giftcarddetails['pregiftcardamount'],"setmyown")==0){
        	$amt=$giftcarddetails['customgiftcardamount'];
        }
        
        
        
        
        $s_name=$giftcarddetails['yourname'];
        $c_message=$giftcarddetails['customermessage'];
            
        $giftcards=GiftCardsTrack::getCardsByOrderId($order->order_id);
        $orderedcards=array();
        foreach($giftcards as $giftcard){
        	$orderedcards[]=$giftcard->card_id;
        }
         
        $giftcardnumbers='';
        if(count($orderedcards)>0){
        	$giftcardnumbers=implode(',', $orderedcards);
        }
       
       	$logourl = $GLOBALS['base_url'] . "/sites/all/commonimages/logo.png";
		$html = '<html>
			<body style="margin=0px; padding:0px;">
			<table  width="95%" style="font-family:verdana,arial,helvetica;font-size:small;border: 1px solid #333333">
			 <tr>
                      <td colspan="2" style="background: #fff; padding: 5px 5px 5px 6px;">
                        <img src="' . $logourl . '" alt="Logo" />
                      </td>
                    </tr>
	  		
	  			
			 <tr>
		        <td colspan="2" bgcolor="#006699" style="color:white">
		          <b>Purchasing Information:</b>
		         </td>
		     </tr>
	  			
	  			
			 <tr>
		    	 <td valign="top" width="50%">
		   		<b>Billing Address:</b><br>
		    		' . $order->billing_first_name . ' ' . $order->billing_last_name . '<br>
		    		' . $order->billing_street1 . '<br>
		     		' . $order->billing_city . ', ' . uc_zone_get_by_id($order->billing_zone) . ' ' . $order->billing_postal_code . '<br>
		    		' . $order->billing_phone.'<br/>'.$order->primary_email.'
		  		</td>
		    </tr>
    				
    				
		    <tr>
		        <td colspan="2" bgcolor="#006699" style="color:white">
		          <b>Order Summary:</b>
		         </td>
		     </tr>
    				
    		 <tr>
				<td nowrap="">
				<b>Order #:</b> ' . $order->order_id . '
				</td>
    		</tr>

			<tr>
            	<td nowrap="">
  				<b>Order Date: </b>'.date("m-d-Y", $order->created).'
  				</td>
  			</tr>
                                                                		
                                                                		
			<tr>
            	<td nowrap="">
     				<b>Order Total: </b>'.uc_currency_format($order->order_total).'
  				</td>
    		</tr>
  	 		
			<tr>
	        	<td colspan="2" bgcolor="#006699" style="color:white">
	         		 <b>Gift Card Summary:</b>
	         	</td>
     		</tr>
		 <tr>
			<td>
		  	<b>to : '.$r_name.'</b> <br/>
		  	<b>from : '.$s_name.'</b><br/>
		  	<b>message : '.$c_message.'</b><br/>
		  	<b>giftcard value : '.$amt.'</b><br/>
		  	<b>gift card claim code : '.$giftcardnumbers.'</b><br/>
			</td>
		 </tr>
  	 		
	 
  	 		
  	 
     </table>
</html>';
	return $html;
}